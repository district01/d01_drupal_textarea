# d01_drupal_textarea #

Provides a "Limited Text area" widget for the text area field.
With this widget you can choose the text formats that are available per field.
When none are selected it will fallback to the default format. It will also
check the users access to the text format.

With this widget you can make sure a certain textarea field is bound to a certain text format
so users can never input in another format.

# Usage #

Configurable through the UI of the field.

# Release notes #

`1.0`
* Provide a widget for the 'text_long' field where you can configure which text formats are allowed.
